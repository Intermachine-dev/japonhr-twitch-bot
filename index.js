//dotenv module
require('dotenv').config();

// twitch module
const { Client } = require('tmi.js');

const client = new Client({
 options: {debug:true},
 connection: {
  secure: true,
  reconnect: true,
 },
 identity: {
  username: process.env.TWITCH_USER,
  password: process.env.TWITCH_PASS,
 },
 channels: ['japon_hr']
})

//connect
client
 .connect()
 .then(()=> client.say('twitch', 'Hola bienvenidos a el stream de japon'))
 .catch(err => console.log(err))

// followers alert
client.on('', (channel, enabled, minutes) => {
  if(enabled) { 
     console.log('Followers-only mode was enabled without throwing an error.')
  }
})

client.on('connected', (address, port) => {
  client.action(
    process.env.TWITCH_CHANNEL_0,
    `the bot has connected ${address}:${port}`,
  )
})

//subcription
client.on("subscription", function (channel, username, methods ) {
	client.say(channel, `${username} Has subscribed PogChamp ` )
});

client.on("resub", function (channel, username, months, message, userstate, methods) {
	client.say(channel, `${username} Has subscribed for ${months} months in a row PogChamp ` )
});

// saludos
client.on('chat', (channel, tags, message, self) => {

  if (
    message.toLowerCase() === '!hello' ||
    message.toLowerCase() === 'holi' ||
    message.toLowerCase() === 'hola' ||
    message.toLowerCase() === 'holis' ||
    message.toLowerCase() === 'holaaa' ||
    message.toLowerCase() === 'hola!!' ||
    message.toLowerCase() === 'holitha' ||
    message.toLowerCase() === 'holiiiiii' ||
    message.toLowerCase() === 'Hola'
  ) {
    // "@alca, heya!"
    client.say(channel, `@${tags.username}, bienvenido mij@!`)
  }

//test comand
  if (message.toLowerCase() === '!ping') {
    // "!pong @alca"
    client.say(channel, `!pong, @${tags.username}`)
  }

/* help commands */
  if (message.toLowerCase() === '!help-commands') {
    // help comands
    client.say(channel, `
	lista de comandos sin comillas: "!ping !hello !jhr !intermachine
	"`)
  }

// comandos
  if (message.toLowerCase() === '!jhr') {
    // cotepond url
    client.say(channel, `url: japon.com FB: japon1811, IG y twitch: japon_hr twitter: JAPONHR`)
  }

  if (message.toLowerCase() === '!intermachine') {
    // comunidad de codigo
    client.say(channel, `codigo de bot liberado: github.com/intermachine-developers/twitch-chat-bot y comunidad de codigo que promociona el bot: github.com/intermachine-developers`)
  }
})

