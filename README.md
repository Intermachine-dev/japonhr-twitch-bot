# Japon HR chatbot twitch

## spanish version
este bot se realizo para apoyar a la streamer japonHR en Twitch

### modulos utlizados
1. tmi.js
2. dotenv
3. nodemon (para el desarrollo)

requisitos para el apoyo o fork de este proyecto
 > git, node.js es6+
    la terminal de sistema operativo y variables de etorno
    
licencia GPL

## english version
This bot was made to support the Japanese streamer on Twitch

### modules used
1. tmi.js
2. dotenv
3. nodemon (for development)

requirements for the support or fork of this project
> git, node.js, es6 +
 the operating system terminal and env variables

GPL license